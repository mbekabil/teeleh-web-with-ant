import games from './games';

export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const generateLabelName = (fieldName) => {
    let label  = '';
    switch(fieldName) {
        case 'aboutMe':
            label = 'About Me';
            break;
        case 'cloudExperience':
            label = 'Cloud experience';
            break; 
        case 'country':
            label = 'Country';
            break;
        case 'customerCommunicationExperience':
            label = 'Customer communication experience';
            break;
        case 'firstName':
            label = 'First name';
            break;
        case 'futureInterest':
            label = 'Future interest';
            break;
        case 'lastName':
            label = 'Last name';
            break;
        case 'mainProductArea':
            label = 'Main product area';
            break;
        case 'office':
            label = 'Office';
            break;
        case 'skills':
            label = 'Skills';
            break;
        case 'ucnBatch':
            label = 'UCN batch';
            break;
        case 'ucnTrack':
            label = 'UCN track';
            break;
        case 'workHistory':
            label = 'Work history';
            break;
        case 'workingLanguage':
            label = 'Working language';
            break;
        case 'gameId':
            label = 'Game Name';
            break;
        case 'platform':
            label = 'Platform';
            break;
        case 'price':
            label = 'Price';
            break;
        case 'gameStatus':
            label = 'Game Status';
            break;
        case 'adType':
            label = 'Ad Type';
            break;
        case 'moreInfo':
            label = 'More Info';
            break;
        case 'uploadFile':
            label = 'Upload File';
            break;
        default:
            label = fieldName;
            break;
    }
    return label;
}

export const availableGames = () => {
    const gameOptions = games.map( option => (
        { "value": option.Id, "displayValue": option.Name}
    ))
    return [{ "value": null, "displayValue": "Choose game"}].concat( gameOptions);
}

export const getGameParameter = ( gameId, param) => {
    let paramValue = ''
    for(let i=0; i < games.length; i++) {
        if( games[i]['Id'] === gameId ) {
            paramValue = games[i][param]
        }
    }
    return paramValue
}
export const convertToRequestString = (formData) => {
    const postParameter = Object.keys(formData).map((key) => (
         encodeURIComponent(key) + '=' + encodeURIComponent(formData[key])
    )).join('&'); 

    return postParameter;
}

export const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};