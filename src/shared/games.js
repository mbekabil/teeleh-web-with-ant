const games = [
    {
        "Id": 26,
        "Name": "Bioshock: The collection",
        "Image": "http://teelehdev.ir/Image/Games/8963/896318-12-06-02-26-48.jpg",
        "UserScore": 8.4,
        "MetaScore": 84,
        "OnlineCapability": false,
        "Publisher": "2k Games",
        "Developer": "2K Games",
        "ReleaseDate": "2016-09-13T00:00:00",
        "Genres": [
            "Shooter",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One"
        ]
    },
    {
        "Id": 27,
        "Name": "Red Dead Redemption 2",
        "Image": "http://teelehdev.ir/Image/Games/2445/244518-12-06-02-27-12.jpg",
        "UserScore": 9.7,
        "MetaScore": 97,
        "OnlineCapability": true,
        "Publisher": "Rockstar",
        "Developer": "Rockstar",
        "ReleaseDate": "2018-10-26T00:00:00",
        "Genres": [
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 28,
        "Name": "The Last of Us",
        "Image": "http://teelehdev.ir/Image/Games/1923/192318-12-06-02-31-46.jpg",
        "UserScore": 9.1,
        "MetaScore": 95,
        "OnlineCapability": true,
        "Publisher": "Sony",
        "Developer": "Naughty Dog",
        "ReleaseDate": "2013-06-14T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "PS3"
        ]
    },
    {
        "Id": 29,
        "Name": "Uncharted 4: A Thief's End",
        "Image": "http://teelehdev.ir/Image/Games/3162/316218-12-06-02-32-01.jpg",
        "UserScore": 8.3,
        "MetaScore": 93,
        "OnlineCapability": true,
        "Publisher": "Sony",
        "Developer": "Naughty Dog",
        "ReleaseDate": "2016-05-10T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PS4"
        ]
    },
    {
        "Id": 30,
        "Name": "A Way Out",
        "Image": "http://teelehdev.ir/Image/Games/713/71318-12-06-02-32-12.jpg",
        "UserScore": 8.1,
        "MetaScore": 78,
        "OnlineCapability": false,
        "Publisher": "EA",
        "Developer": "Hazelight",
        "ReleaseDate": "2018-03-23T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 31,
        "Name": "Assassin's Creed IV: Black Flag",
        "Image": "http://teelehdev.ir/Image/Games/7757/775718-12-06-02-32-35.jpg",
        "UserScore": 7.9,
        "MetaScore": 83,
        "OnlineCapability": true,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft",
        "ReleaseDate": "2013-11-12T00:00:00",
        "Genres": [
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 33,
        "Name": "Assassin’s Creed Unity",
        "Image": "http://teelehdev.ir/Image/Games/9480/948018-12-06-02-33-08.jpg",
        "UserScore": 5,
        "MetaScore": 70,
        "OnlineCapability": true,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft ",
        "ReleaseDate": "2014-11-11T00:00:00",
        "Genres": [
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 34,
        "Name": "Assassin’s Creed Syndicate",
        "Image": "http://teelehdev.ir/Image/Games/7678/767818-12-06-02-33-30.jpg",
        "UserScore": 6.8,
        "MetaScore": 76,
        "OnlineCapability": false,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft Quebec",
        "ReleaseDate": "2015-12-23T00:00:00",
        "Genres": [
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 37,
        "Name": "Assassin’s Creed Origins",
        "Image": "http://teelehdev.ir/Image/Games/6271/627118-12-06-02-33-46.png",
        "UserScore": 7.2,
        "MetaScore": 81,
        "OnlineCapability": false,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft",
        "ReleaseDate": "2017-10-27T00:00:00",
        "Genres": [
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 38,
        "Name": "Battlefield 4",
        "Image": "http://teelehdev.ir/Image/Games/459/45918-12-06-02-34-03.jpg",
        "UserScore": 7,
        "MetaScore": 85,
        "OnlineCapability": true,
        "Publisher": "EA",
        "Developer": "EA DICE",
        "ReleaseDate": "2013-11-12T00:00:00",
        "Genres": [
            "Shooter",
            "Action RPG"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 39,
        "Name": "Battlefield Hardline",
        "Image": "http://teelehdev.ir/Image/Games/4971/497118-12-06-02-34-34.jpg",
        "UserScore": 5.1,
        "MetaScore": 73,
        "OnlineCapability": true,
        "Publisher": "EA",
        "Developer": "Visceral Games",
        "ReleaseDate": "2013-03-17T00:00:00",
        "Genres": [
            "Shooter",
            "Action RPG"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 40,
        "Name": "Battlefield 1",
        "Image": "http://teelehdev.ir/Image/Games/4126/412618-12-06-02-34-51.jpg",
        "UserScore": 8,
        "MetaScore": 89,
        "OnlineCapability": true,
        "Publisher": "EA",
        "Developer": "EA DICE",
        "ReleaseDate": "2016-10-18T00:00:00",
        "Genres": [
            "Shooter",
            "Action RPG"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 41,
        "Name": "BloodBorne",
        "Image": "http://teelehdev.ir/Image/Games/3111/311118-12-06-02-35-12.jpeg",
        "UserScore": 8.9,
        "MetaScore": 92,
        "OnlineCapability": true,
        "Publisher": "Sony",
        "Developer": "Software",
        "ReleaseDate": "2015-03-24T00:00:00",
        "Genres": [
            "Shooter",
            "Fantasy",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4"
        ]
    },
    {
        "Id": 42,
        "Name": "Call of Duty Advanced Warfare",
        "Image": "http://teelehdev.ir/Image/Games/6116/611618-12-06-02-36-05.jpg",
        "UserScore": 5.7,
        "MetaScore": 83,
        "OnlineCapability": true,
        "Publisher": "Activision",
        "Developer": "Sledgehammer",
        "ReleaseDate": "2014-11-03T00:00:00",
        "Genres": [
            "Shooter",
            "Action RPG"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 43,
        "Name": "Call Duty Black ops III",
        "Image": "http://teelehdev.ir/Image/Games/122/12218-12-06-02-36-22.jpg",
        "UserScore": 4.8,
        "MetaScore": 81,
        "OnlineCapability": true,
        "Publisher": "Activision",
        "Developer": "Treyarch",
        "ReleaseDate": "2015-11-06T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 44,
        "Name": "Call of Duty Ghosts",
        "Image": "http://teelehdev.ir/Image/Games/4500/450018-12-06-02-36-43.jpg",
        "UserScore": 3.8,
        "MetaScore": 78,
        "OnlineCapability": true,
        "Publisher": "Activision",
        "Developer": "Infinity Ward",
        "ReleaseDate": "2013-11-15T00:00:00",
        "Genres": [
            "Shooter",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 45,
        "Name": "Call of Duty Infinite Warfare",
        "Image": "http://teelehdev.ir/Image/Games/2827/282718-12-06-02-36-59.jpg",
        "UserScore": 3.7,
        "MetaScore": 77,
        "OnlineCapability": true,
        "Publisher": "Activision",
        "Developer": "Infinity Ward",
        "ReleaseDate": "2016-11-04T00:00:00",
        "Genres": [
            "Shooter",
            "Action RPG"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 46,
        "Name": "Crash Bandicoot N. Sane Trilogy",
        "Image": "http://teelehdev.ir/Image/Games/1565/156518-12-06-02-37-12.jpg",
        "UserScore": 6.7,
        "MetaScore": 78,
        "OnlineCapability": false,
        "Publisher": "Activision",
        "Developer": "VicariousV",
        "ReleaseDate": "2018-06-29T00:00:00",
        "Genres": [
            "Competitive"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 47,
        "Name": "Dark Souls Remastered",
        "Image": "http://teelehdev.ir/Image/Games/5818/581818-12-06-02-37-30.jpg",
        "UserScore": 6.1,
        "MetaScore": 86,
        "OnlineCapability": true,
        "Publisher": "Namco",
        "Developer": "From Software",
        "ReleaseDate": "2018-05-25T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 48,
        "Name": "Call of Duty WWII",
        "Image": "http://teelehdev.ir/Image/Games/8618/861818-12-06-02-37-46.jpg",
        "UserScore": 4.2,
        "MetaScore": 79,
        "OnlineCapability": true,
        "Publisher": "Activision",
        "Developer": "Sledgehammer G",
        "ReleaseDate": "2017-11-03T00:00:00",
        "Genres": [
            "Shooter",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 49,
        "Name": "Dark Souls III",
        "Image": "http://teelehdev.ir/Image/Games/8291/829118-12-06-02-38-12.jpg",
        "UserScore": 8.9,
        "MetaScore": 89,
        "OnlineCapability": true,
        "Publisher": "Namco",
        "Developer": "From Software",
        "ReleaseDate": "2016-04-12T00:00:00",
        "Genres": [
            "Action RPG"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 50,
        "Name": "Destiny The Collection",
        "Image": "http://teelehdev.ir/Image/Games/66/6618-12-06-02-38-42.png",
        "UserScore": 7.1,
        "MetaScore": 76,
        "OnlineCapability": true,
        "Publisher": "Activision",
        "Developer": "Bungie",
        "ReleaseDate": "2016-09-20T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 51,
        "Name": "Detroit: Become Human",
        "Image": "http://teelehdev.ir/Image/Games/7226/722618-12-06-02-38-59.jpg",
        "UserScore": 8.8,
        "MetaScore": 78,
        "OnlineCapability": false,
        "Publisher": "Sony",
        "Developer": "Quantic Dream",
        "ReleaseDate": "2018-05-25T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PS4"
        ]
    },
    {
        "Id": 52,
        "Name": "Deus Ex Mankind Divided",
        "Image": "http://teelehdev.ir/Image/Games/3521/352118-12-06-02-39-20.jpg",
        "UserScore": 7.6,
        "MetaScore": 84,
        "OnlineCapability": false,
        "Publisher": "Square Enix",
        "Developer": " Eidos Montreal",
        "ReleaseDate": "2016-08-23T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One"
        ]
    },
    {
        "Id": 53,
        "Name": "Dishonored 2",
        "Image": "http://teelehdev.ir/Image/Games/7914/791418-12-06-02-39-38.jpg",
        "UserScore": 7.9,
        "MetaScore": 88,
        "OnlineCapability": false,
        "Publisher": "Bethesda",
        "Developer": "Arkane Studios",
        "ReleaseDate": "2016-11-10T00:00:00",
        "Genres": [
            "Shooter",
            "Action-adventure"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4"
        ]
    },
    {
        "Id": 54,
        "Name": "Dishonored Death of outsider",
        "Image": "http://teelehdev.ir/Image/Games/8289/828918-12-06-02-39-49.jpg",
        "UserScore": 7.2,
        "MetaScore": 82,
        "OnlineCapability": false,
        "Publisher": "Bethesda",
        "Developer": "Arkane Studios",
        "ReleaseDate": "2017-09-15T00:00:00",
        "Genres": [
            "Shooter",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One"
        ]
    },
    {
        "Id": 55,
        "Name": "Dishonored definitive edition",
        "Image": "http://teelehdev.ir/Image/Games/1816/181618-12-06-02-39-59.jpg",
        "UserScore": 7.4,
        "MetaScore": 80,
        "OnlineCapability": false,
        "Publisher": "Bethesda",
        "Developer": "Arkane Studios",
        "ReleaseDate": "2018-08-15T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4"
        ]
    },
    {
        "Id": 56,
        "Name": "DOOM",
        "Image": "http://teelehdev.ir/Image/Games/1582/158218-12-06-02-40-11.jpg",
        "UserScore": 8.4,
        "MetaScore": 85,
        "OnlineCapability": false,
        "Publisher": "Bethesda",
        "Developer": "Id Software",
        "ReleaseDate": "2016-05-03T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One"
        ]
    },
    {
        "Id": 57,
        "Name": "DriveClub",
        "Image": "http://teelehdev.ir/Image/Games/5077/507718-12-06-02-40-26.jpg",
        "UserScore": 6.2,
        "MetaScore": 71,
        "OnlineCapability": true,
        "Publisher": "Sony",
        "Developer": "Evolution Stud",
        "ReleaseDate": "2014-10-07T00:00:00",
        "Genres": [
            "Vehicle simulation",
            "Racing",
            "Competitive"
        ],
        "Platforms": [
            "PS4"
        ]
    },
    {
        "Id": 58,
        "Name": "The Evil within 2",
        "Image": "http://teelehdev.ir/Image/Games/9316/931618-12-06-02-40-40.jpg",
        "UserScore": 8.8,
        "MetaScore": 76,
        "OnlineCapability": false,
        "Publisher": "Bethesda",
        "Developer": "Tango Gameworks",
        "ReleaseDate": "2017-10-13T00:00:00",
        "Genres": [
            "Survival"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 59,
        "Name": "The Evil within",
        "Image": "http://teelehdev.ir/Image/Games/8316/831618-12-06-02-41-01.jpg",
        "UserScore": 7.3,
        "MetaScore": 79,
        "OnlineCapability": false,
        "Publisher": "Bethesda",
        "Developer": "Tango Gameworks",
        "ReleaseDate": "2014-10-14T00:00:00",
        "Genres": [
            "Survival horror",
            "Action-adventure"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 60,
        "Name": "Far cry 5",
        "Image": "http://teelehdev.ir/Image/Games/7532/753218-12-06-02-41-26.jpg",
        "UserScore": 6.8,
        "MetaScore": 81,
        "OnlineCapability": true,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft Montral",
        "ReleaseDate": "2018-03-27T00:00:00",
        "Genres": [
            "Shooter",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 61,
        "Name": "FIFA 18",
        "Image": "http://teelehdev.ir/Image/Games/5239/523918-12-06-02-41-41.jpg",
        "UserScore": 3.3,
        "MetaScore": 84,
        "OnlineCapability": true,
        "Publisher": "EA",
        "Developer": " EA Vancouver",
        "ReleaseDate": "2017-09-26T00:00:00",
        "Genres": [
            "Sports-based fighting"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 62,
        "Name": "Final Fantasy XV",
        "Image": "http://teelehdev.ir/Image/Games/7610/761018-12-06-02-41-54.jpg",
        "UserScore": 7.5,
        "MetaScore": 81,
        "OnlineCapability": true,
        "Publisher": "Square Enix",
        "Developer": "Square Enix",
        "ReleaseDate": "2016-11-29T00:00:00",
        "Genres": [
            "Action RPG",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 63,
        "Name": " Fallout 4",
        "Image": "http://teelehdev.ir/Image/Games/2744/274418-12-06-02-42-03.jpg",
        "UserScore": 6.7,
        "MetaScore": 87,
        "OnlineCapability": false,
        "Publisher": "Bethesda",
        "Developer": "Bethesda",
        "ReleaseDate": "2016-11-10T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 64,
        "Name": "Tom Clancy's Ghost Recon Wildlands",
        "Image": "http://teelehdev.ir/Image/Games/9315/931518-12-06-02-42-27.jpg",
        "UserScore": 6.1,
        "MetaScore": 70,
        "OnlineCapability": true,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft Paris",
        "ReleaseDate": "2017-03-07T00:00:00",
        "Genres": [
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 65,
        "Name": "God of War",
        "Image": "http://teelehdev.ir/Image/Games/7871/787118-12-06-02-42-48.jpg",
        "UserScore": 9.2,
        "MetaScore": 94,
        "OnlineCapability": false,
        "Publisher": "Sony",
        "Developer": "SCE Santa Monic",
        "ReleaseDate": "2018-04-20T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "PS3"
        ]
    },
    {
        "Id": 66,
        "Name": "Grand Theft Auto V",
        "Image": "http://teelehdev.ir/Image/Games/5511/551118-12-06-02-43-02.jpg",
        "UserScore": 8.3,
        "MetaScore": 97,
        "OnlineCapability": true,
        "Publisher": "RockStar",
        "Developer": "Rockstar North",
        "ReleaseDate": "2014-11-18T00:00:00",
        "Genres": [
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One"
        ]
    },
    {
        "Id": 67,
        "Name": "Assassin's Creed Odyssey",
        "Image": "http://teelehdev.ir/Image/Games/5557/555718-12-06-02-43-17.jpg",
        "UserScore": 5.6,
        "MetaScore": 83,
        "OnlineCapability": false,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft",
        "ReleaseDate": "2018-10-02T00:00:00",
        "Genres": [
            "Action RPG",
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4"
        ]
    },
    {
        "Id": 68,
        "Name": "Assassin's Creed Rogue",
        "Image": "http://teelehdev.ir/Image/Games/1783/178318-12-06-02-29-32.jpg",
        "UserScore": 5.9,
        "MetaScore": 71,
        "OnlineCapability": false,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft",
        "ReleaseDate": "2018-03-20T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One"
        ]
    },
    {
        "Id": 69,
        "Name": "Batman: Arkham Knight",
        "Image": "http://teelehdev.ir/Image/Games/109/10918-12-06-02-43-44.jpg",
        "UserScore": 7.8,
        "MetaScore": 87,
        "OnlineCapability": false,
        "Publisher": "Warner Bros.",
        "Developer": "Rocksteady",
        "ReleaseDate": "2015-06-23T00:00:00",
        "Genres": [
            "Fantasy",
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4"
        ]
    },
    {
        "Id": 70,
        "Name": "Call of Duty: Black Ops 4",
        "Image": "http://teelehdev.ir/Image/Games/2401/240118-12-06-02-44-02.jpg",
        "UserScore": 4.4,
        "MetaScore": 85,
        "OnlineCapability": true,
        "Publisher": "Activision",
        "Developer": "Treyarch",
        "ReleaseDate": "2018-10-12T00:00:00",
        "Genres": [
            "Shooter"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One"
        ]
    },
    {
        "Id": 71,
        "Name": " Dark Souls II: Scholar of the First Sin",
        "Image": "http://teelehdev.ir/Image/Games/5019/501918-12-06-02-44-20.jpg",
        "UserScore": 8,
        "MetaScore": 87,
        "OnlineCapability": true,
        "Publisher": "Namco",
        "Developer": "From Software",
        "ReleaseDate": "2015-04-07T00:00:00",
        "Genres": [
            "Action RPG",
            "Open world"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4"
        ]
    },
    {
        "Id": 72,
        "Name": "Destiny 2",
        "Image": "http://teelehdev.ir/Image/Games/6483/648318-12-06-02-44-34.jpg",
        "UserScore": 4.9,
        "MetaScore": 85,
        "OnlineCapability": true,
        "Publisher": "Activision",
        "Developer": "Bungie",
        "ReleaseDate": "2017-09-06T00:00:00",
        "Genres": [
            "Shooter",
            "First-person party-based RPG",
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One"
        ]
    },
    {
        "Id": 73,
        "Name": "Far Cry Primal",
        "Image": "http://teelehdev.ir/Image/Games/1188/118818-12-06-02-44-52.jpg",
        "UserScore": 6.4,
        "MetaScore": 76,
        "OnlineCapability": false,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft ",
        "ReleaseDate": "2016-02-23T00:00:00",
        "Genres": [
            "Shooter",
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4"
        ]
    },
    {
        "Id": 74,
        "Name": "Far Cry 4",
        "Image": "http://teelehdev.ir/Image/Games/5591/559118-12-06-02-45-07.jpg",
        "UserScore": 7.7,
        "MetaScore": 85,
        "OnlineCapability": true,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft",
        "ReleaseDate": "2014-11-18T00:00:00",
        "Genres": [
            "Shooter",
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One",
            "PS3",
            "XBOX 360"
        ]
    },
    {
        "Id": 75,
        "Name": "God of War III Remastered",
        "Image": "http://teelehdev.ir/Image/Games/9450/945018-12-06-02-45-20.jpg",
        "UserScore": 8.1,
        "MetaScore": 81,
        "OnlineCapability": false,
        "Publisher": "Sony",
        "Developer": "Wholesale",
        "ReleaseDate": "2015-07-14T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "PS3"
        ]
    },
    {
        "Id": 76,
        "Name": "Heavy Rain & Beyond: Two Souls Collection",
        "Image": "http://teelehdev.ir/Image/Games/7682/768218-12-06-02-45-30.jpg",
        "UserScore": 8.3,
        "MetaScore": 78,
        "OnlineCapability": false,
        "Publisher": "Sony",
        "Developer": "Quantic Dream",
        "ReleaseDate": "2016-03-04T00:00:00",
        "Genres": [
            "Interactive movie"
        ],
        "Platforms": [
            "PS4"
        ]
    },
    {
        "Id": 77,
        "Name": "Hitman 2",
        "Image": "http://teelehdev.ir/Image/Games/6814/681418-12-06-02-45-40.jpg",
        "UserScore": 7.4,
        "MetaScore": 81,
        "OnlineCapability": true,
        "Publisher": "Warner Bros.",
        "Developer": "IO Interactive",
        "ReleaseDate": "2018-11-09T00:00:00",
        "Genres": [
            "Stealth",
            "Action-adventure",
            "Action",
            "Tactical shooter"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 78,
        "Name": "Hitman",
        "Image": "http://teelehdev.ir/Image/Games/6522/652218-12-06-03-11-32.jpg",
        "UserScore": 7.9,
        "MetaScore": 84,
        "OnlineCapability": false,
        "Publisher": "Warner Bros.",
        "Developer": "IO Interactive",
        "ReleaseDate": "2016-10-31T00:00:00",
        "Genres": [
            "Stealth",
            "Action-adventure",
            "Action"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 79,
        "Name": "Until Dawn",
        "Image": "http://teelehdev.ir/Image/Games/706/70618-12-06-02-45-53.jpg",
        "UserScore": 8.3,
        "MetaScore": 79,
        "OnlineCapability": false,
        "Publisher": "Sony",
        "Developer": "SuperMassive Games",
        "ReleaseDate": "2015-08-25T00:00:00",
        "Genres": [
            "Survival horror",
            "Interactive movie"
        ],
        "Platforms": [
            "PS4"
        ]
    },
    {
        "Id": 80,
        "Name": "The Witcher 3: Wild Hunt",
        "Image": "http://teelehdev.ir/Image/Games/5799/579918-12-06-02-46-04.jpg",
        "UserScore": 9.3,
        "MetaScore": 93,
        "OnlineCapability": false,
        "Publisher": "Warner Bros.",
        "Developer": "CD Projekt Red",
        "ReleaseDate": "2015-05-18T00:00:00",
        "Genres": [
            "Action RPG",
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 81,
        "Name": "FIFA 19",
        "Image": "http://teelehdev.ir/Image/Games/1418/141818-12-06-02-46-21.jpg",
        "UserScore": 1.8,
        "MetaScore": 83,
        "OnlineCapability": true,
        "Publisher": "EA",
        "Developer": "EA Sports",
        "ReleaseDate": "2018-09-25T00:00:00",
        "Genres": [
            "Competitive"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4",
            "PS3",
            "XBOX 360",
            "Nintendo Switch"
        ]
    },
    {
        "Id": 82,
        "Name": "The Crew",
        "Image": "http://teelehdev.ir/Image/Games/4444/444418-12-06-02-46-32.jpg",
        "UserScore": 5.3,
        "MetaScore": 61,
        "OnlineCapability": true,
        "Publisher": "Ubisoft",
        "Developer": "Ivory Tower Ubisoft",
        "ReleaseDate": "2014-12-02T00:00:00",
        "Genres": [
            "Racing",
            "Open world"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC",
            "XBOX 360"
        ]
    },
    {
        "Id": 83,
        "Name": "The Crew 2",
        "Image": "http://teelehdev.ir/Image/Games/4637/463718-12-06-02-46-44.jpg",
        "UserScore": 4.9,
        "MetaScore": 64,
        "OnlineCapability": true,
        "Publisher": "Ubisoft",
        "Developer": "Ivory Tower Ubisoft",
        "ReleaseDate": "2018-06-26T00:00:00",
        "Genres": [
            "Racing",
            "Open world"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 84,
        "Name": "Dying Light",
        "Image": "http://teelehdev.ir/Image/Games/4641/464118-12-06-02-46-54.jpg",
        "UserScore": 7.9,
        "MetaScore": 75,
        "OnlineCapability": true,
        "Publisher": "Warner Bros.",
        "Developer": "Techland",
        "ReleaseDate": "2015-01-27T00:00:00",
        "Genres": [
            "Shooter",
            "Survival",
            "Survival horror",
            "Open world",
            "Action-adventure",
            "Action",
            "First person shooter"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 85,
        "Name": "DmC: Devil May Cry Definitive Edition",
        "Image": "http://teelehdev.ir/Image/Games/671/67118-12-06-02-47-06.jpg",
        "UserScore": 7.4,
        "MetaScore": 83,
        "OnlineCapability": false,
        "Publisher": "Capcom",
        "Developer": "Ninja Theory",
        "ReleaseDate": "2015-03-10T00:00:00",
        "Genres": [
            "Fantasy",
            "Hack & slash",
            "Action"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4",
            "XBOX 360",
            "PS3"
        ]
    },
    {
        "Id": 86,
        "Name": "Horizon Zero Dawn",
        "Image": "http://teelehdev.ir/Image/Games/2378/237818-12-06-02-47-18.jpg",
        "UserScore": 8.3,
        "MetaScore": 89,
        "OnlineCapability": false,
        "Publisher": "Sony",
        "Developer": "Guerrilla",
        "ReleaseDate": "2017-02-28T00:00:00",
        "Genres": [
            "Action RPG"
        ],
        "Platforms": [
            "PS4"
        ]
    },
    {
        "Id": 87,
        "Name": "Injustice 2",
        "Image": "http://teelehdev.ir/Image/Games/4557/455718-12-06-02-47-31.jpg",
        "UserScore": 8.2,
        "MetaScore": 87,
        "OnlineCapability": true,
        "Publisher": "Warner Bros",
        "Developer": "NetherRealm Studios",
        "ReleaseDate": "2017-05-16T00:00:00",
        "Genres": [
            "Competitive"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PC"
        ]
    },
    {
        "Id": 88,
        "Name": "Mafia III",
        "Image": "http://teelehdev.ir/Image/Games/8402/840218-12-06-02-47-43.jpg",
        "UserScore": 5.2,
        "MetaScore": 68,
        "OnlineCapability": false,
        "Publisher": "2K",
        "Developer": "Hangar 13",
        "ReleaseDate": "2016-10-07T00:00:00",
        "Genres": [
            "Open world",
            "Action"
        ],
        "Platforms": [
            "PC",
            "XBOX One",
            "PS4"
        ]
    },
    {
        "Id": 89,
        "Name": "NBA 2K 18",
        "Image": "http://teelehdev.ir/Image/Games/5248/524818-12-06-02-47-56.jpg",
        "UserScore": 1.7,
        "MetaScore": 80,
        "OnlineCapability": true,
        "Publisher": "2K",
        "Developer": "Visual Concepts",
        "ReleaseDate": "2017-09-15T00:00:00",
        "Genres": [
            "Sports-based fighting"
        ],
        "Platforms": [
            "PS4",
            "XBOX One",
            "PS3",
            "XBOX 360",
            "Nintendo Switch"
        ]
    },
    {
        "Id": 90,
        "Name": " Nier Automata",
        "Image": "http://teelehdev.ir/Image/Games/2423/242318-12-06-02-48-06.jpg",
        "UserScore": 8.8,
        "MetaScore": 88,
        "OnlineCapability": false,
        "Publisher": "Square Enix",
        "Developer": "PlatinumGames",
        "ReleaseDate": "2017-03-07T00:00:00",
        "Genres": [
            "Action RPG"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4"
        ]
    },
    {
        "Id": 91,
        "Name": "Overwatch",
        "Image": "http://teelehdev.ir/Image/Games/5940/594018-12-06-02-48-16.jpg",
        "UserScore": 6.7,
        "MetaScore": 91,
        "OnlineCapability": true,
        "Publisher": "Blizzard Entertainment",
        "Developer": "Blizzard Entertainment",
        "ReleaseDate": "2016-05-23T00:00:00",
        "Genres": [
            "Action",
            "First person shooter"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One"
        ]
    },
    {
        "Id": 92,
        "Name": "Rise of the Tomb Raider: 20 Year Celebration",
        "Image": "http://teelehdev.ir/Image/Games/7898/789818-12-06-02-48-27.jpg",
        "UserScore": 8.1,
        "MetaScore": 88,
        "OnlineCapability": true,
        "Publisher": "Square Enix",
        "Developer": "Crystal Dynamics, Nixxes",
        "ReleaseDate": "2016-10-11T00:00:00",
        "Genres": [
            "Action-adventure"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4"
        ]
    },
    {
        "Id": 93,
        "Name": " Pro Evolution Soccer 2018",
        "Image": "http://teelehdev.ir/Image/Games/2601/260118-12-06-02-48-37.jpg",
        "UserScore": 7.2,
        "MetaScore": 83,
        "OnlineCapability": true,
        "Publisher": "Konami",
        "Developer": "Konami",
        "ReleaseDate": "2017-09-12T00:00:00",
        "Genres": [
            "Sports-based fighting"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One",
            "Nintendo Switch",
            "XBOX 360",
            "PS3"
        ]
    },
    {
        "Id": 94,
        "Name": " Watch Dogs",
        "Image": "http://teelehdev.ir/Image/Games/6955/695518-12-06-02-48-50.jpg",
        "UserScore": 6.3,
        "MetaScore": 80,
        "OnlineCapability": true,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft Montreal",
        "ReleaseDate": "2017-05-14T00:00:00",
        "Genres": [
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4",
            "PS3",
            "XBOX 360"
        ]
    },
    {
        "Id": 95,
        "Name": "Watch dogs 2",
        "Image": "http://teelehdev.ir/Image/Games/2143/214318-12-06-02-48-58.jpg",
        "UserScore": 7.8,
        "MetaScore": 82,
        "OnlineCapability": true,
        "Publisher": "Ubisoft",
        "Developer": "Ubisoft Montreal",
        "ReleaseDate": "2016-11-15T00:00:00",
        "Genres": [
            "Open world",
            "Action-adventure"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One"
        ]
    },
    {
        "Id": 96,
        "Name": "Wolfenstein: The New Order ",
        "Image": "http://teelehdev.ir/Image/Games/7438/743818-12-06-02-49-13.jpg",
        "UserScore": 8.1,
        "MetaScore": 79,
        "OnlineCapability": false,
        "Publisher": "Bethesda",
        "Developer": "MachineGames",
        "ReleaseDate": "2014-05-20T00:00:00",
        "Genres": [
            "Action",
            "First person shooter"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4"
        ]
    },
    {
        "Id": 97,
        "Name": "Wolfenstein II: The New Colossus",
        "Image": "http://teelehdev.ir/Image/Games/8887/888718-12-06-02-49-21.jpg",
        "UserScore": 7.5,
        "MetaScore": 87,
        "OnlineCapability": false,
        "Publisher": "Bethesda",
        "Developer": "MachineGames",
        "ReleaseDate": "2017-10-27T00:00:00",
        "Genres": [
            "Action",
            "First person shooter"
        ],
        "Platforms": [
            "PS4",
            "PC",
            "XBOX One"
        ]
    },
    {
        "Id": 98,
        "Name": "WWE 2K18",
        "Image": "http://teelehdev.ir/Image/Games/5501/550118-12-06-02-49-32.jpg",
        "UserScore": 5.6,
        "MetaScore": 66,
        "OnlineCapability": true,
        "Publisher": "2K",
        "Developer": "Yuke's",
        "ReleaseDate": "2017-10-13T00:00:00",
        "Genres": [
            "Sports-based fighting"
        ],
        "Platforms": [
            "XBOX One",
            "PC",
            "PS4",
            "XBOX 360",
            "PS3"
        ]
    }
]

export default games;