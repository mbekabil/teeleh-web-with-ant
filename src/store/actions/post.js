import * as actionTypes from './actionTypes'; 
import db from '../../firebase';

const createPostStart = () => {
    return {
        type: actionTypes.CREATE_POST_START
    }
}

const createPostSuccess = (postId) => {
    return {
        type: actionTypes.CREATE_POST_SUCCESS,
        postId: postId
    }
};

const createPostFail = (error) => {
    return {
        type: actionTypes.CREATE_POST_FAIL, 
        error: error
    }
};

const fetchPostsStart = () => {
    return {
        type: actionTypes.FETCH_POSTS_START
    }
}

const fetchPostsSuccess = (posts) => {
    return {
        type: actionTypes.FETCH_POSTS_SUCCESS,
        posts: posts
    }
};

const fetchPostsFail = (error) => {
    return {
        type: actionTypes.FETCH_POSTS_FAIL,
        error: error
    }
};

export const createPost = (data) => {
    return dispatch => {
        dispatch(createPostStart()); 
        db.collection('posts').add(data)
        .then(function(docRef) {
            dispatch(createPostSuccess(docRef.id));
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
            dispatch(createPostFail(error));
        });
    }
};

export const fetchAllPosts = () => {
    return dispatch => {
        dispatch(fetchPostsStart()); 
        db.collection('posts').get() 
        .then( snapshot => {
            if( snapshot && snapshot.docs ) {
                const posts = snapshot.docs.map( post => (
                    post.data()
                ));
                dispatch(fetchPostsSuccess(posts))
            }
        })
        .catch(err => {
            console.log("error", err);
            dispatch(fetchPostsFail(err));
        });
    }
}
