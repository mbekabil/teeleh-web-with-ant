import * as actionTypes from './actionTypes'; 
import db from '../../firebase'; 

const createProfileStart = () => {
    return {
        type: actionTypes.CREATE_PROFILE_START
    }
}

const createProfileSuccess = (profileId) => {
    return {
        type: actionTypes.CREATE_PROFILE_SUCCESS,
        profileId: profileId
    }
};

const createProfileFail = (error) => {
    return {
        type: actionTypes.CREATE_PROFILE_FAIL, 
        error: error
    }
};

const fetchProfileStart = () => {
    return {
        type: actionTypes.FETCH_PROFILE_START
    }
}

const fetchProfileSuccess = (profile) => {
    return {
        type: actionTypes.FETCH_PROFILE_SUCCESS,
        profile: profile
    }
};

const fetchProfileFail = (error) => {
    return {
        type: actionTypes.FETCH_PROFILE_FAIL,
        error: error
    }
};
const fetchProfilesStart = () => {
    return {
        type: actionTypes.FETCH_PROFILES_START
    }
}

const fetchProfilesSuccess = (profiles) => {
    return {
        type: actionTypes.FETCH_PROFILES_SUCCESS,
        profiles: profiles
    }
};

const fetchProfilesFail = (error) => {
    return {
        type: actionTypes.FETCH_PROFILES_FAIL,
        error: error
    }
};

export const createProfile = (profileData) => {
    return dispatch => {
        dispatch(createProfileStart()); 
        const userId = localStorage.getItem('userId');
        db.collection('profile').doc(userId).set(profileData)
        .then(function() {
            dispatch(createProfileSuccess(userId));
            dispatch(fetchProfile(userId));
            // const expirationDate = new Date(new Date().getTime() + profileRef.data.expiresIn * 1000);
            // localStorage.setItem('token', profileRef.data.idToken);
            // localStorage.setItem('expirationDate', expirationDate);
            // localStorage.setItem('profileId', profileRef.data.localId);
        })
        .catch(function(error) {
            console.error("Error writing document: ", error);
            dispatch(createProfileFail(error));
        });
    }
};

export const fetchProfile = (id) => { 
    return dispatch => {
        dispatch(fetchProfileStart()); 
        db.collection('gameDetails').doc(id).get()
        .then( doc => {
            if (doc.exists) {
                dispatch(fetchProfileSuccess(doc.data()));
            } else {
                dispatch(fetchProfileFail());
            }
        })
        .catch(err => {
            console.log("error", err);
            dispatch(fetchProfileFail(err));
        });
    }
}

export const fetchAllProfiles = () => {
    return dispatch => {
        dispatch(fetchProfilesStart()); 
        db.collection('games').get() 
        .then( snapshot => {
            if( snapshot && snapshot.docs ) {
                const profiles = snapshot.docs.map( profile => (
                    profile.data()
                ));
                dispatch(fetchProfilesSuccess(profiles))
            }
        })
        .catch(err => {
            console.log("error", err);
            dispatch(fetchProfilesFail(err));
        });

       
    }
}

export const profileCheckState = () => {
    return dispatch =>  {
        const userId = localStorage.getItem('userId');
        if( userId ) {
            dispatch(fetchProfile(userId));
        }
    }
};
