import * as actionTypes from '../actions/actionTypes'; 
import {updateObject} from '../../shared/utility'; 

const initialState = {
    profiles: null, 
    profile: null,
    profileId: '',
    loading: false,
    created: false,
    error: false,
}

const createProfileStart = (state) => {
    return updateObject( state, { loading: true } );
}
const createProfileSuccess = (state, action) => {
    return updateObject( state, {
        profileId: action.profileId, 
        loading: false, 
        created: true,
        error: false,
    });
}

const createProfileFail = (state) => {
    return updateObject( state, { error: true, loading: false } );
};

const fetchProfileStart = ( state) => {
    return updateObject( state, { loading: true});
}

const fetchProfileSuccess = (state, action) => {
    return updateObject( state, {
        profile: action.profile,
        loading: false, 
        error: false,
    });
}

const fetchProfileFail = (state) => {
    return updateObject( state, { error: true, loading: false } );
};

const fetchProfilesStart = ( state) => {
    return updateObject( state, { loading: true});
}

const fetchProfilesSuccess = (state, action) => {
    return updateObject( state, {
        profiles: action.profiles, 
        loading: false, 
        error: false,
    });
}

const fetchProfilesFail = (state) => {
    return updateObject( state, { error: true, loading: false } );
};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.CREATE_PROFILE_START: return createProfileStart(state, action);
        case actionTypes.CREATE_PROFILE_SUCCESS: return createProfileSuccess(state, action);
        case actionTypes.CREATE_PROFILE_FAIL: return createProfileFail(state, action); 
        case actionTypes.FETCH_PROFILE_START: return fetchProfileStart(state, action); 
        case actionTypes.FETCH_PROFILE_SUCCESS: return fetchProfileSuccess(state, action); 
        case actionTypes.FETCH_PROFILE_FAIL: return fetchProfileFail(state, action);
        case actionTypes.FETCH_PROFILES_START: return fetchProfilesStart(state, action); 
        case actionTypes.FETCH_PROFILES_SUCCESS: return fetchProfilesSuccess(state, action); 
        case actionTypes.FETCH_PROFILES_FAIL: return fetchProfilesFail(state, action);       
        default: return state;
    }
};

export default  reducer;
