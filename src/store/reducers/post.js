import * as actionTypes from '../actions/actionTypes'; 
import {updateObject} from '../../shared/utility'; 

const initialState = {
    postId: '',
    posts: null,
    loading: false,
    created: false,
    error: false,
}

const createPostStart = (state) => {
    return updateObject( state, { loading: true } );
}
const createPostSuccess = (state, action) => {
    return updateObject( state, {
        postId: action.postId, 
        loading: false, 
        created: true,
        error: false,
    });
}

const createPostFail = (state) => {
    return updateObject( state, { error: true, loading: false } );
};

const fetchPostsStart = ( state) => {
    return updateObject( state, { loading: true});
}

const fetchPostsSuccess = (state, action) => {
    return updateObject( state, {
        posts: action.posts, 
        loading: false, 
        error: false,
    });
}

const fetchPostsFail = (state) => {
    return updateObject( state, { error: true, loading: false } );
};


const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.CREATE_POST_START: return createPostStart(state, action);
        case actionTypes.CREATE_POST_SUCCESS: return createPostSuccess(state, action);
        case actionTypes.CREATE_POST_FAIL: return createPostFail(state, action);  
        case actionTypes.FETCH_POSTS_START: return fetchPostsStart(state, action); 
        case actionTypes.FETCH_POSTS_SUCCESS: return fetchPostsSuccess(state, action); 
        case actionTypes.FETCH_POSTS_FAIL: return fetchPostsFail(state, action);     
        default: return state;
    }
};

export default  reducer;
