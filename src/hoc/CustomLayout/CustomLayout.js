import React, {Component} from 'react';
import {connect} from 'react-redux';
import { Layout} from 'antd';

import NavMenu from './Navigation/NavMenu';
import Logo from '../../components/Logo/Logo';
import HorizontalMenu from './Navigation/HorizontalMenu';
import './CustomLayout.css'; 

const { Header, Content, Footer, Sider } = Layout;
class CustomLayout extends Component {
    render() {
        const sidebar = (
            <Sider className='Sidebar'>
                <Logo alignRight='true'/>
                <NavMenu isAuthenticated={this.props.isAuthenticated}/>
            </Sider>
        )
        return ( 
            <Layout>
                { !this.props.isFullwidth ? sidebar : '' }
                <Layout className={ !this.props.isFullwidth ? 'LayoutWithSidebar' : ''}>
                    <Header className='Header'> 
                        { this.props.isFullwidth ? <Logo /> : '' } 
                        Teeleh Web 
                        <div className='HorizontalMenu'>
                            <HorizontalMenu isAuthenticated={this.props.isAuthenticated} />
                        </div>
                    </Header>
                    <Content className='ContentContainer'>
                        <div className='Content'>
                            {this.props.children}
                        </div>
                    </Content>
                    <Footer className={ !this.props.isFullwidth ? 'Footer FooterInSideBar' : 'Footer'}>
                        Teeleh Web ©2018
                    </Footer>
                </Layout>
            </Layout>
        )    
    }
}

const mapStateToPros = state => {
    return {
        isAuthenticated: state.auth.token !== null,
        hasProfile: state.profile.profile !== null
    }
};

export default connect(mapStateToPros)(CustomLayout);
