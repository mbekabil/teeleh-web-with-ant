import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Menu, Icon, Button } from 'antd';

const SubMenu = Menu.SubMenu;

class NavMenu extends Component {
  state = {
    collapsed: false,
  }

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }
  handleClick = (e) => {
    let path = '/';
    switch( e.key ) {
      case '1':
        path = '/games'; 
        break;
      case '2':
          path = this.props.isAuthenticated ? '/logout' : '/login'; 
          break;
      case '5':
          path = '/posts'; 
          break;
      case '6':
        path = '/post/create'; 
        break;
      default: 
        path = '#';
        break;
    }
    this.props.history.push(path);
  }

  render() {
    let account = (
      <Menu.Item key="2">
          <Icon type="login"/>
          <span>Login</span>
      </Menu.Item>
    )
    let createAd = ''
    if( this.props.isAuthenticated ) {
      account = (
        <Menu.Item key="2">
            <Icon type="logout"/>
            <span>Logout</span>
        </Menu.Item>
      )
      createAd =  <Menu.Item key="6">Create Post</Menu.Item>
    }
    return (
      <div >
        <Button type="primary" onClick={this.toggleCollapsed} style={{ marginBottom: 16, marginTop: 25 }}>
            <Icon type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'} />
        </Button>
        <Menu
            onClick={this.handleClick}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
            theme="dark"
            inlineCollapsed={this.state.collapsed}
        >
          <Menu.Item key="1">
                <Icon type="appstore"/>
                <span>Games</span>
          </Menu.Item>
          <SubMenu key="sub1" title={<span><Icon type="hdd"/><span>Advertisement</span></span>}>
                <Menu.Item key="5">View Posts</Menu.Item>
                {createAd}
          </SubMenu>
          {account}
        </Menu>
      </div>
    );
  }
}

export default withRouter(NavMenu);