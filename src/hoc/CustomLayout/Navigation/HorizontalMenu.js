import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import { Menu, Icon } from 'antd';

const SubMenu = Menu.SubMenu;

class HorizontalMenu extends Component {
    state = {
        current: 'mail',
    }

    handleClick = (e) => {
        let path = '/';
        switch( e.key ) {
            case '1':
                path = this.props.isAuthenticated ? '/logout' : '/login'; 
                break;
            case '2':
                path = '/edit'; 
                break;
            default: 
                path = '#';
            break;
        }
        this.props.history.push(path);
    }

    render() {
        let accountMenu = ( <Menu.Item key="1">
                                <Icon type="login"/>
                                <span>Login</span>
                            </Menu.Item>
                        )
        if( this.props.isAuthenticated){
            accountMenu = ( <SubMenu key="sub1" title={<span><Icon type="user"/><span>My Profile</span></span>}>
                                <Menu.Item key="2">
                                    <Icon type="edit"/>
                                    <span>Edit</span>
                                </Menu.Item>
                                <Menu.Item key="1">
                                    <Icon type="logout"/>
                                    <span>Logout</span>
                                </Menu.Item>
                            </SubMenu>
                        )
        }
        return (
        <Menu
            onClick={this.handleClick}
            selectedKeys={[this.state.current]}
            mode="horizontal"
            theme="dark"
        >
            { accountMenu }
        </Menu>
        );
    }
}
export default withRouter(HorizontalMenu);
