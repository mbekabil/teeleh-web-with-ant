import React from 'react';
import {NavLink} from 'react-router-dom';

import './Logo.css';
import logoIcon from '../../assets/images/joystick.png';

const logo = (props) => (
    <NavLink to='/'>
        <div className='Logo'>
            <img src={logoIcon}  alt='Teeleh web'/>
        </div>
    </NavLink>
);

export default logo;
