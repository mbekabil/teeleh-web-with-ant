import React from 'react';
import classes from './Spinner.css';

export const spinner = () => <div className={classes.Loader}>Loading...</div>
