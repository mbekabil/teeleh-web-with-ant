import React from 'react';
import './ProfilePicture.css';


const addPlaceholderImage = (e) => {
    e.target.src = 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQC4sHIq_on7n5GnYO6e4D2R0OLdDXrA8lOe3-vPrWddZd7z7br&usqp=CAU';
};

const profilePicture = ({ small, imgUrl, altText, width}) => (
    <div className={small ? 'ProfilePicture Small ' : 'ProfilePicture'}>
        <img 
            src={imgUrl} 
            onError={addPlaceholderImage} 
            alt={altText ? altText : 'Profile Picture'} 
            width={width}
            />
    </div>
);
export default profilePicture;
