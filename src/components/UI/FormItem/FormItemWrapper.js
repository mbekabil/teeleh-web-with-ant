import React from 'react';
import { Form, Input, DatePicker, Select, Upload, Icon, Button } from 'antd';

import './FormItemWrapper.css';

const formItemWrapper = ( props ) => {
    const FormItem = Form.Item;
    const { RangePicker } = DatePicker;
    const { 
        form, 
        isFullWidth,
        elementType,
        elementConfig,
        changed,
        validation,
        defaultValues,
        label, 
        id,
    } = props;
    const { getFieldDecorator } = form;
    const { TextArea } = Input;
    const Option = Select.Option;
    const uploadProps = {
        action: 'gs://teeleh-88bb1.appspot.com',
        listType: 'picture',
    };
    let inputElement = null;
    const inputClasses = ['InputElement'];
    let inputContainerClasses = ['Input']; 
    inputClasses.push(elementType);
    
    if( isFullWidth ) {
        inputClasses.push('FullWidth');
        inputContainerClasses.push('FullWidth');
    }

    if(elementConfig.type === 'hidden' ) {
        inputContainerClasses.push('hidden'); 
    }
    
    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
    };
    switch ( elementType ) {
        case 'input':
            inputElement = <Input type={elementConfig.type} />
            break;
        case 'date':
            inputElement = elementConfig.type === 'range' ? (
                    <RangePicker />
                ) : (
                    <DatePicker />
                );
            break;
        case 'textarea':
            inputElement = <TextArea 
                                rows={elementConfig.rows ? elementConfig.rows : 8} 
                                columns={elementConfig.columns ? elementConfig.columns : 8}/>
            break;
        case 'select':
            inputElement = (
                <Select 
                    mode={elementConfig.isMultiple ? 'multiple' : 'default'}
                    onChange={changed}>
                    {elementConfig.options.map((option, index) => (
                            <Option 
                                key={index} 
                                value={option.value}>
                                {option.displayValue}
                            </Option>
                    ))}
                </Select>    
            );
            break;
        case 'dragger':
            inputElement = (
                <Upload {...uploadProps}>
                    <Button>
                    <Icon type="upload" /> Upload
                    </Button>
                </Upload>
            )
            break;
        default:
            break;
    }
    let options = {rules: [{ 
                    required: validation.required, 
                    message: elementConfig.message 
                    }],
                }

    if( typeof defaultValues !== 'undefined' ) {
        options['initialValue'] = defaultValues;
    }

    return (
        <div className={inputContainerClasses.join(' ')}>
            <FormItem
                    {...formItemLayout}
                    label={label ? label : ''}
                >
                {getFieldDecorator(id, options)(inputElement)}
            </FormItem>
        </div>
    );

};

export default formItemWrapper;
