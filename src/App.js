import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import LoginForm from './containers/Auth/LoginForm';
import RegistrationForm from './containers/Auth/RegistrationForm';
import Logout from './containers/Auth/Logout/Logout';
import ViewProfile from './containers/Profiles/Profile/ViewProfile/ViewProfile';
import Profiles from './containers/Profiles/Profiles';
import CreatePost from './containers/Posts/Post/CreatePost/CreatePost';
import Posts from './containers/Posts/Posts';
import CustomLayout from './hoc/CustomLayout/CustomLayout';
import LandingPage from './containers/LandingPage/index'

import * as actions from './store/actions';

class App extends Component {
    componentDidMount () {
        this.props.onTryAutoSignup();
        this.props.onTryAutoFetchProfile();
    }

    render () {
        let isFullwidth = false; 
        if( this.props.location.pathname === '/' ||  (this.props.location.pathname === '/login' && this.props.isAuthenticated)) {
            isFullwidth = true
        }
        let routes = (
            <Switch>
                <Route path="/login" component={LoginForm} />
                <Route path="/register" component={RegistrationForm} />
                <Route exact path="/game/:id" component={ViewProfile}/>  
                <Route path="/games" component={Profiles} /> 
                <Route path="/posts" component={Posts} /> 
                <Route path="/post/create" component={CreatePost} />
                <Route path="/" component={LandingPage} /> 
                <Redirect to="/" />
            </Switch>
        );
        if ( this.props.isAuthenticated ) {
                routes = (
                    <Switch>
                        <Route path="/logout" component={Logout} />
                        <Route exact path="/game/:id" component={ViewProfile}/> 
                        <Route path="/post/create" component={CreatePost} />
                        <Route path="/posts" component={Posts} /> 
                        <Route path="/games" component={Profiles} /> 
                        <Route path="/" component={LandingPage} />
                        <Redirect to="/" />
                    </Switch>
                );
        }

        return (
            <div>
              <CustomLayout isFullwidth={isFullwidth} >
                {routes}  
              </CustomLayout>
            </div>
        );
    }
}
const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null, 
        hasProfile: state.profile.profile !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onTryAutoSignup: () => dispatch( actions.authCheckState() ), 
        onTryAutoFetchProfile: () => dispatch( actions.profileCheckState() )
    };
};

export default withRouter( connect( mapStateToProps, mapDispatchToProps )( App ) );
