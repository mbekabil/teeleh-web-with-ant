import React, { Component } from 'react';
import {NavLink} from 'react-router-dom';
import { Icon, Carousel } from 'antd';

import './index.css';

class LandingPage extends Component {
    
    render() {
        return (
            <div className='LandingPageContainer'>
                    <div className='HeaderInfo '> 
                        <Carousel autoplay>
                            <div><h1>Welcome to Teeleh Web</h1></div>
                            <div><h1>A smart solution to share your secondhand console games</h1></div>
                            <div><h1>Sell, buy or exchange games</h1></div>
                        </Carousel>
                    </div>
                
                <div className='NavigationLinks'>
                    <div className='NavigationLink'> 
                        <NavLink
                            to="/games"
                            exact
                            activeClassName='active'>
                            <Icon type="appstore"/> All Games
                            <div className="Description" > 
                                List of all available console games
                            </div>
                        </NavLink> 
                    </div>
                    <div className='NavigationLink'> 
                        <NavLink
                            to="/posts"
                            exact
                            activeClassName='active'>
                            <Icon type="hdd"/> Secondhand Games 
                            <div className="Description" > 
                                List of available Secondhand games 
                            </div>
                        </NavLink>
                    </div>
                    <div className='NavigationLink'>
                        <NavLink
                            to="/post/create"
                            exact
                            activeClassName='active'>
                            <Icon type="solution"/> Post your games
                            <div className="Description" > 
                                Create advertisement post
                            </div>                 
                        </NavLink>
                    </div>
                </div>
            </div>
        )
    }
}

export default LandingPage;
