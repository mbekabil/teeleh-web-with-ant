import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Checkbox } from 'antd';

import * as actions from '../../store/actions/index';
import './Auth.css';

const FormItem = Form.Item;

class LoginForm extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onAuth(values.email, values.password,true);
      }
    });
  }
  componentDidMount() {
    if( this.props.authRedirectPath !== '/' || this.props.authRedirectPath !== '/login' ) {
        this.props.onSetAuthRedirectPath();
    }
  }
  
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} style={{maxWidth: 500, paddingRight: 0, marginLeft: '20%'}} className="login-form">
        <FormItem>
            {getFieldDecorator('email', {
                rules: [{
                  type: 'email', message: 'The input is not valid E-mail!',
                  }, {
                  required: true, message: 'Please input your E-mail!',
              }],
            })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} type="email" placeholder="Email" />
          )}
        </FormItem>
        <FormItem>
            {getFieldDecorator('password', {
                rules: [{ required: true, message: 'Please input your Password!' }],
            })(
                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
          )}
        </FormItem>
        <FormItem>
            {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
            })(
                <Checkbox style={{ float: 'left'}}>Remember me</Checkbox>
          )}
          <a className="login-form-forgot" href="/reset">Forgot password</a>
          <Button type="primary" htmlType="submit" style={{ width: 'inherit', clear: 'both', display: 'block'}}className="login-form-button">
            Log in
          </Button>
          Or <a href="/register">register now!</a>
        </FormItem>
      </Form>
    );
  }
}

const WrappedLoginForm = Form.create()(LoginForm);

const mapStateToProps = state => {
  return {
      loading: state.auth.loading,
      error: state.auth.error,
      isAuthenticated: state.auth.token !== null,
      authRedirectPath: state.auth.authRedirectPath, 
  }
};


const mapDispatchToProps = dispatch => {
  return {
      onAuth: (email, password, isSignup) => dispatch(actions.auth(email, password, isSignup)),
      onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/'))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WrappedLoginForm);
