import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Form, Input, Tooltip, Icon, Checkbox, Button, Alert } from 'antd';
import ReactPhoneInput from 'react-phone-input-2';

import * as actions from '../../store/actions/index';

const FormItem = Form.Item;

class RegistrationForm extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.onAuth(values.email, values.password, true);
      }
    });
  }

  handleConfirmBlur = (e) => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  }

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
        form.validateFields(['confirm'], { force: true });
    }
    callback();
  }
  renderError = () => {
      return (<Alert
        message="Error"
        description="Unable to register user. Please, check if your password satisfies the minimum length requirement!"
        type="error"
        closable
      />)
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
            span: 24,
            offset: 0,
            },
            sm: {
            span: 16,
            offset: 8,
            },
        },
    };
    return (
        <Form onSubmit={this.handleSubmit}>
            { this.props.error ? this.renderError() : ''}
            <FormItem
                {...formItemLayout}
                label="First Name" 
            >
                {getFieldDecorator('firstName', {
                    rules: [{ required: true, message: 'Please input your first name!' }],
                })(
                    <Input />
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="Last Name" 
            >
                {getFieldDecorator('lastName', {
                    rules: [{ required: true, message: 'Please input your last name!' }],
                })(
                    <Input />
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label={(
                    <span>
                        Username&nbsp;
                        <Tooltip title="What do you want others to call you?">
                            <Icon type="question-circle-o" />
                        </Tooltip>
                    </span>
                )}
            >
                {getFieldDecorator('username', {
                    rules: [{ required: false, message: 'Please input your username!', whitespace: true }],
                })(
                <Input />
            )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="E-mail"
            >
                {getFieldDecorator('email', {
                    rules: [{
                        type: 'email', message: 'The input is not valid E-mail!',
                        }, {
                        required: true, message: 'Please input your E-mail!',
                    }],
                })(
                    <Input />
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="Password"
                help="Password should be minimum of 6 characters"
            >
                {getFieldDecorator('password', {
                    rules: [{
                        required: true, message: 'Please input your password!',
                        }, {
                        validator: this.validateToNextPassword
                    }],
                })(
                    <Input type="password" />
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="Confirm Password"
            >
                {getFieldDecorator('confirm', {
                    rules: [{
                        required: true, message: 'Please confirm your password!',
                    }, {
                        validator: this.compareToFirstPassword,
                    }],
                })(
                    <Input type="password" onBlur={this.handleConfirmBlur} />
                )}
            </FormItem>
            <FormItem
                {...formItemLayout}
                label="Phone Number"
            >
                {getFieldDecorator('phone', {
                    rules: [{ 
                        required: false, 
                        message: 'Please input your phone number!' 
                    }],
                })(
                    <ReactPhoneInput defaultCountry={'fi'}  />
                )}
            </FormItem>
            <FormItem 
                {...tailFormItemLayout}
            >
                {getFieldDecorator('agreement', {
                    valuePropName: 'checked',
                    rules: [{ 
                        required: true, 
                        message: 'Please agree to the Terms and conditions!' 
                    }]
                })(
                    <Checkbox>I have read the <a href="/agreement">agreement</a></Checkbox>
            )}
            </FormItem>
            <FormItem {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">Register</Button>
            </FormItem>
        </Form>
    );
}
}

const WrappedRegistrationForm = Form.create()(RegistrationForm);

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        error: state.auth.error,
        isAuthenticated: state.auth.token !== null,
        authRedirectPath: state.auth.authRedirectPath, 
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onAuth: (formData, isSignup) => dispatch(actions.auth(formData, isSignup)),
        onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath('/'))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WrappedRegistrationForm);
