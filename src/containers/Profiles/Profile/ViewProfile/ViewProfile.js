import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Spin, Rate, Tag} from 'antd';
import moment from 'moment';

import ProfilePicture from '../../../../components/UI/ProfilePicture/ProfilePircture';  
import './ViewProfile.css';
import * as actions from '../../../../store/actions/index';

class ViewProfile extends Component {

    componentDidMount() {
        if(this.props.match.params.id) {
            this.props.onFetchProfile(this.props.match.params.id);
        }
    }
    
    render () { 
        let returnHtml = ''
        const profile = this.props.profile;
        
        if(this.props.loading) {
            returnHtml = <Spin size='large' tip="Loading..." />
        } else if( profile ) {
            returnHtml = (
                <div className="ViewProfileContainer"> 
                    <ProfilePicture imgUrl={profile.Image} />
                    <div className="Name">
                        <h1> {profile.Name}</h1>
                    </div>
                    <div className="Info">
                        <div>
                            <label>Publisher:</label>
                            <span> {profile.Publisher}</span>
                        </div>
                        <div>
                            <label>Developer:</label>
                            <span> {profile.Developer}</span>
                        </div>
                        <div>
                            <label> ReleaseDate: </label>
                            <span> {moment(profile.ReleaseDate).format('DD/MM/YYYY')}</span>
                        </div>
                        <div>
                            <label>Availability:</label>
                            <span> {profile.OnlineCapability === true ? 'Availabile online' : 'Available on store  '}</span>
                        </div>
                        <div>
                            <label>User Score:</label>
                            <span> {<Rate disabled allowHalf defaultValue={profile.UserScore/2} />} {profile.UserScore}/10</span>
                        </div>
                        <div>
                            <label>Meta Score:</label>
                            <span> {<Rate disabled allowHalf defaultValue={profile.MetaScore/20} />} {profile.MetaScore}/100</span>
                        </div>
                        <div>
                            <label>Supported Platforms:</label>
                            <span> { profile.Platforms.map( (platform, index) =>( <Tag key={index} color="#1890ff">{platform}</Tag>))}</span>
                        </div>
                        <div>
                            <label>Gener:</label>
                            <span> { profile.Genres.map( (gener, index) =>( <Tag key={index} color="#1890ff">{gener}</Tag>))} </span>
                        </div>
                    </div>
                </div>
            )
        } 
        return (
            <div className="ViewProfile"> 
                {returnHtml}
            </div>
        )
    }
}
const mapStateToPros = state => {
    return {
        profile: state.profile.profile, 
        profileId: state.profile.profileId,
        loading: state.profile.loading, 
        created: state.profile.created, 
        error: state.profile.error
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onFetchProfile: (id) => dispatch(actions.fetchProfile(id)),
    }
};

export default connect(mapStateToPros, mapDispatchToProps)(ViewProfile);
