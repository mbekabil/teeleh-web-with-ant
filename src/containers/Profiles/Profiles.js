import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect, withRouter} from 'react-router-dom';
import { Card, Spin, Rate, Skeleton } from 'antd';

import * as actions from '../../store/actions/index'; 
import axios from '../../axios';
import './Profiles.css';

class Profiles extends Component {
    state = {
        selectedProfile: null 
    }

    profileClickedHandler = (profile) => {
        this.setState( {selectedProfile: profile});
    }; 

    componentDidMount() {
        this.props.onFetchAllProfiles(); 
    }
    render () {
        let returnHtml = <Spin size='large' tip="Loading..." />
        if( this.state.selectedProfile ) {
            return <Redirect to={{
                pathname: '/game/' + this.state.selectedProfile.Id,
                state: { selectedProfile: this.state.selectedProfile.data }
            }} />
        }
        if(this.props.profiles) {
            returnHtml = this.props.profiles.map( (profile) => {
                const thumbnail = profile.Image; 
                const { Meta } = Card;
                const description = (
                    <div>
                        <div className='InfoSection'></div>
                        <div className='Description'>
                            <span>{<Rate disabled allowHalf defaultValue={profile.UserScore/2} />}</span>
                            <span> <b>User Score: </b>{profile.UserScore}/10</span>
                            <span> <b>Platform: </b>{ profile.Platforms.join(', ')} </span>
                            <span> <b>Genre: </b>{ profile.Genres.join(', ')} </span>
                        </div>
                    </div>
                )
                return(
                    <div onClick={(id) => this.profileClickedHandler(profile)} className='Profile' key={profile.Id}>
                        <Card
                            hoverable
                            style={{ width: 240, height: 460, borderRadius: 8 }}
                            cover={<img alt={profile.Name} src={thumbnail}/>}
                        >
                            <Skeleton loading={this.props.loading} avatar active>
                                <Meta
                                title={profile.Name}
                                description={description}
                                />
                            </Skeleton>
                        </Card>
                    </div>
                )
            })
        }
        return (
            <div className='Profiles'>
                {returnHtml}
            </div>
        )
    }
}
const mapStateToPros = state => {
    return {
        profiles: state.profile.profiles, 
        profile: state.profile.profile, 
        profileId: state.profile.profileId,
        loading: state.profile.loading, 
        created: state.profile.created, 
        error: state.profile.error
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onFetchAllProfiles: () => dispatch(actions.fetchAllProfiles()),
    }
};

export default withRouter( connect(mapStateToPros, mapDispatchToProps)(Profiles, axios));
