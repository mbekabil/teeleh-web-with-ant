import React, {Component} from 'react'
import { Form, List, Icon, Spin, Tag } from 'antd';
import {connect} from 'react-redux';
import { withRouter} from 'react-router-dom';

import FormItemWrapper from '../../components/UI/FormItem/FormItemWrapper'
import ProfilePicture from '../../components/UI/ProfilePicture/ProfilePircture';
import {availableGames, generateLabelName} from '../../shared/utility';
import * as actions from '../../store/actions/index'; 
import './Posts.css';

class Posts extends Component {
    state = {
        filters: {
            gameId: {
                elementType: 'select',
                elementConfig: {
                    options: availableGames(),
                    isMultiple: false,
                    message: 'Please select a game!'
                },
                selectedOption: null,
                validation: {
                    required: false
                }
            },
            platform: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: '', displayValue: 'Choose platform'},
                        {value: 'xbox', displayValue: 'X-Box'},
                        {value: 'ps4', displayValue: 'PS4'},
                    ],
                    isMultiple: true,
                    message: 'Please select a platform!'
                },
                selectedOption: [],
                validation: {
                    required: false
                }
            }
        },
        selectedTags: []
    }
    componentDidMount = () => {
        this.props.onFetchAllPosts();
    }
    filterChangeHandler = (value, formId) => { 
        const updatedControls = {
            ...this.state.filters,
            [formId]: {
                ...this.state.filters[formId],
                selectedOption: value
            }
        };
        this.setState({filters: updatedControls});  
    }

    filterByPlatform = (posts, platforms) => {
        let filteredPosts = []
        for (let i=0; i < posts.length; i ++) {
            if (platforms.length === 1 ) {
                if( posts[i].platform.includes(platforms[0])) {
                    filteredPosts.push(posts[i])
                }
            } else if ( platforms.length === 2 ) {
                if( posts[i].platform.includes(platforms[0]) && posts[i].platform.includes(platforms[1])) {
                    filteredPosts.push(posts[i])
                }
            }
        }
        return filteredPosts;
    } 

    filterByGameName = (posts, gameId) => {
        let filteredPosts = []
        for( let i=0; i < posts.length; i ++) {
            if(posts[i].gameId === gameId) {
                filteredPosts.push(posts[i])
            }
        }
        return filteredPosts;
    }
        
    filteredPosts = () => {
        const gameId = this.state.filters.gameId.selectedOption
        const platforms = this.state.filters.platform.selectedOption
        const {posts} = this.props 
        let filteredPosts = []
    
        if (gameId !== null && platforms.length !== 0) {
            filteredPosts = this.filterByGameName(posts, gameId)
            filteredPosts = this.filterByPlatform(filteredPosts, platforms)       
        } else if (gameId !== null && platforms.length === 0) {
            filteredPosts = this.filterByGameName(posts, gameId)
        } else if (gameId === null && platforms.length !== 0) {
            filteredPosts = this.filterByPlatform(posts, platforms)
        } else {
            filteredPosts = posts
        }
        return filteredPosts;
    }
    tagClickHandler = ( tag, checked ) => {
        const { selectedOption } = this.state.filters.platform;
        const nextSelectedTags = checked
            ? [...selectedOption, tag]
            : selectedOption.filter(t => t !== tag);
        const updatedFilters = {
            ...this.state.filters,
            platform: {
                ...this.state.filters.platform,
                selectedOption: nextSelectedTags
            }
        };
        this.setState({filters: updatedFilters});
        this.props.form.setFieldsValue({platform: nextSelectedTags}) 
    }
    renderFilters = () => {
        const formElementsArray = [];
        for ( let key in this.state.filters ) {
            formElementsArray.push( {
                id: key,
                config: this.state.filters[key]
            } );
        }
        return formElementsArray.map( formElement => (
                <FormItemWrapper
                    key={formElement.id}
                    id={formElement.id}
                    isFullWidth
                    defaultValues={formElement.config.selectedOption}
                    elementType={formElement.config.elementType}
                    elementConfig={formElement.config.elementConfig}
                    validation={formElement.config.validation}
                    form={this.props.form}
                    changed={(value) => this.filterChangeHandler(value, formElement.id)}
                    label={generateLabelName(formElement.id)} />
            ));
    }
    renderPosts = () => {
        const { CheckableTag } = Tag;
        return (<List itemLayout="vertical"
                size="large"
                pagination={{
                    onChange: (page) => {},
                    pageSize: 8,
                }}
                dataSource={this.filteredPosts()}
                renderItem={(item, index) => {
                    const description =  ( 
                                <div key={index}>
                                    <div><b>Game Status: </b>{item.gameStatus} </div>
                                    <div><b>Price: </b>{item.price}€ </div>
                                    <div><b>Payment Type: </b>{item.adType} </div>
                                    <div><b>Supported Platforms: </b>
                                        { item.platform.map( (platform, index) => ( 
                                            <CheckableTag 
                                                onChange={checked => this.tagClickHandler(platform, checked)}
                                                checked={this.state.filters.platform.selectedOption.indexOf(platform) > -1}
                                                key={index}>
                                                    {platform}
                                            </CheckableTag>))
                                        }
                                    </div>
                                    <div><b><Icon type="mail"/></b> {item.email} </div>
                                </div>
                    );
                    return (
                            <List.Item
                                key={index}
                                extra={<ProfilePicture altText="logo" imgUrl={item.cover} />}
                                >
                                <List.Item.Meta
                                    title={<a href={item.href}>{item.gameName}</a>}
                                    description={item.moreInfo}/>
                                {description}
                            </List.Item>
                        )
                    }
                } />
            )
    }
    render() {
        console.log('inrender', this.state)
        return (
            <div className='Posts'>
                {this.props.loading || !this.props.posts ? 
                    <Spin size='large' tip="Loading..." /> :
                    <div className="PostsWrapper" >
                        <div className="Filters" >
                            <h3>Filter posts </h3>
                            {this.renderFilters()}    
                        </div>
                        <div>
                            {this.renderPosts()}
                        </div>
                    </div>
                }
            </div>
        )
    }
}
const PostsWithFormWrapper = Form.create()(Posts);
const mapStateToPros = state => {
    return {
        posts: state.post.posts, 
        loading: state.post.loading, 
        error: state.post.error
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onFetchAllPosts: () => dispatch(actions.fetchAllPosts()),
    }
};

export default withRouter( connect(mapStateToPros, mapDispatchToProps)(PostsWithFormWrapper));
