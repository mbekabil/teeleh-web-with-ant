import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect, withRouter} from 'react-router-dom';
import { Form, Button, Spin, Alert } from 'antd';

import FormItemWrapper from '../../../../components/UI/FormItem/FormItemWrapper';
import {generateLabelName, tailFormItemLayout, availableGames, getGameParameter} from '../../../../shared/utility'; 
import * as actions from '../../../../store/actions/index';
import axios from '../../../../axios'; 
import './CreatePost.css';

class CreatePost extends Component {
    state = {
        controls: {
            gameId: {
                elementType: 'select',
                elementConfig: {
                    options: availableGames(),
                    isMultiple: false,
                    message: 'Please select a game!'
                },
                selectedOption: null,
                validation: {
                    required: true
                }
            },
            platform: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: '', displayValue: 'Choose platform'},
                        {value: 'xbox', displayValue: 'X-Box'},
                        {value: 'ps4', displayValue: 'PS4'},
                    ],
                    isMultiple: true,
                    message: 'Please select a platform!'
                },
                selectedOption: null,
                validation: {
                    required: true
                }
            },
            price: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    placeholder: 'Price in €',
                    message: 'Please input the price!'
                },
                value: '',
                validation: {
                    required: true
                }
            },
            gameStatus: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: '', displayValue: 'Choose game status'},
                        {value: 'old', displayValue: 'Old'},
                        {value: 'new', displayValue: 'New'},
                    ],
                    isMultiple: false,
                    message: 'Please select the game status!'
                },
                selectedOption: null,
                validation: {
                    required: true
                }
            },
            adType: {
                elementType: 'select',
                elementConfig: {
                    options: [
                        {value: '', displayValue: 'Choose advertisement type'},
                        {value: 'cash', displayValue: 'Cash only'},
                        {value: 'exchange', displayValue: 'Exchange only'},
                        {value: 'both', displayValue: 'Cash and Exchange'},
                    ],
                    isMultiple: false,
                    message: 'Please select the advertisement type!'
                },
                selectedOption: null,
                validation: {
                    required: true
                }
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'emailaddress@domain',
                    message: 'Please input your email address!'
                },
                value: '',
                validation: {
                    required: true
                }
            },
            moreInfo: {
                elementType: 'textarea',
                elementConfig: {
                    type: 'text',
                    placeholder: 'More information',
                },
                value: '',
                validation: {
                    required: false
                }
            },
            uploadFile: {
                elementType: 'dragger',
                elementConfig: {
                    type: 'dragger',
                    placeholder: 'Price in €',
                    message: 'Please input the price!'
                },
                value: '',
                validation: {
                    required: false
                }
            },
        }
    };
    renderError = () => {
        return (<Alert
          message="Error"
          description="Unable to create post. Please try again"
          type="error"
          closable
        />)
    }

    renderSuccessMessage= () => {
        return (<Alert
          message="Success!"
          description="Post created successfully"
          type="success"
          closable
        />)
    }
    createPostHandler = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
                const gameId = values['gameId'];
                values['gameName'] = getGameParameter(gameId, 'Name')
                values['cover'] = getGameParameter(gameId, 'Image')
                values['uploadFile'] = ''
                this.props.onCreatePost(values);
          }
        });
      }
    render() {
        let customForm = '';     
        const formElementsArray = [];
        const FormItem = Form.Item;
        for ( let key in this.state.controls ) {
            formElementsArray.push( {
                id: key,
                config: this.state.controls[key]
            } );
        }
        customForm = formElementsArray.map( formElement => {
            return (
                <FormItemWrapper
                    key={formElement.id}
                    id={formElement.id}
                    isFullWidth
                    defaultValues={formElement.config.value}
                    elementType={formElement.config.elementType}
                    elementConfig={formElement.config.elementConfig}
                    validation={formElement.config.validation}
                    form={this.props.form}
                    label={generateLabelName(formElement.id)} />
            )
        });
        if( !this.props.isAuthenticated ) {
            return <Redirect to={{pathname: '/login'}} />
        }

        if( this.props.loading) {
            return <Spin size='large' tip="Loading..." />
        } else {
            return (
                <div className='CreatePost'>
                    <h2>Create advertisement post</h2>
                    <Form onSubmit={this.createPostHandler}>
                        { this.props.error ? this.renderError() : this.props.created ? this.renderSuccessMessage() : ''}
                        {customForm}
                        <FormItem {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit">Save</Button>
                        </FormItem>
                    </Form>
                </div>
            )
        }
    }
}
const CreatePostForm = Form.create()(CreatePost);
const mapStateToPros = state => {
    return {
         postId: state.post.postId,
         games: state.profile.profiles, 
         loading: state.post.loading, 
         created: state.post.created, 
         error: state.post.error,
         isAuthenticated: state.auth.token !== null
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onCreatePost: (data) => dispatch(actions.createPost(data)),
    }
};

export default withRouter(connect(mapStateToPros, mapDispatchToProps)(CreatePostForm, axios));
